﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class SpriteCycler : MonoBehaviour
{
    [SerializeField]
    Sprite[] _sprites;
    [SerializeField]
    float _fps = 24;
    [SerializeField]
    int _offset;

    // Update is called once per frame
    void Update()
    {
        this.GetComponent<SpriteRenderer>().sprite = _sprites[Mathf.FloorToInt(((Time.time * _fps) + _offset) % _sprites.Length)];
    }
}
