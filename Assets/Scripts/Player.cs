﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField]
    float _speed = 10;
    [SerializeField]
    float _maxSpeed = 10;
    [SerializeField]
    Vector2 dir;
    [SerializeField]
    float _jumpForce = 40f;
    [SerializeField]
    float _initialJumpForce = 100f;
    [SerializeField]
    float _maxJumpTime = 0.2f;

    float _wallJumpDir;

    float _jumpCounter = 0;

    bool canJump = false;

    bool isGrounded = false;

    bool isJumping = true;

    [SerializeField]
    GameObject _playerSoul;
    [SerializeField]
    LayerMask _rayCastLayers;

    Rigidbody2D _rigidbody;

    Vector3 _respawnLoc;
    private bool canWallJump = false;

    [SerializeField]
    GameEvent _deathEvent;

    // Start is called before the first frame update
    void Start()
    {
        _respawnLoc = transform.position;
        _rigidbody = GetComponent<Rigidbody2D>();

        if (Camera.main.GetComponent<CameraFollowScript>()._targetFollow != this.gameObject)
        {
            Camera.main.GetComponent<CameraFollowScript>().SetPlayer(this);
        }
    }

    // Update is called once per frame
    void Update()
    {
        dir = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

        var hit = Physics2D.Raycast(transform.position, Vector2.down, GetComponent<PolygonCollider2D>().bounds.extents.y + 0.01f,  _rayCastLayers);

        if (hit.transform != null)
        {
            Debug.Log("Tester raycast");

            isGrounded = true;
        }

        if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
        {
            _rigidbody.AddForce(new Vector2(0, _initialJumpForce), ForceMode2D.Impulse);
            isGrounded = false;
            isJumping = true;
        }
        else if (Input.GetKeyDown(KeyCode.Space) && canWallJump)
        {
            _rigidbody.AddForce(new Vector2(_wallJumpDir, _initialJumpForce), ForceMode2D.Impulse);

        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            isJumping = false;
        }


        if (isGrounded)
        {
            _rigidbody.AddForce(new Vector2(dir.x * _speed * Time.deltaTime, 0), ForceMode2D.Impulse);
        }
        else
        {
            _rigidbody.AddForce(new Vector2(dir.x * _speed * Time.deltaTime * 0.3f, 0), ForceMode2D.Impulse);
        }

        _rigidbody.velocity = new Vector2(Mathf.Clamp(_rigidbody.velocity.x, -_maxSpeed, _maxSpeed), _rigidbody.velocity.y);
    }

    [Button]
    public void Kill()
    {
        _deathEvent.Invoke();
        Instantiate(_playerSoul, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.ToLower() == "floor")
        {
            foreach (var col in collision.contacts)
            {
                Debug.Log(col.normal);
            }
            if (collision.contacts.Any(t => Mathf.RoundToInt(t.normal.y) == 1))
            {
                //Hitting Walkable Floor

                Debug.Log("Tester floor");

                canJump = true;
                isGrounded = true;
                canWallJump = false;
                _jumpCounter = 0;
            }
            else if (collision.contacts.Any(t => Mathf.RoundToInt(t.normal.x) == -1 || Mathf.RoundToInt(t.normal.x) == 1))
            {
                //Presuming we're on a wall

                Debug.Log("Tester wall");

                if (collision.contacts.Any(t => Mathf.RoundToInt(t.normal.x) == -1))
                {
                    _wallJumpDir = -1000;
                }
                else
                {
                    _wallJumpDir = 1000;
                }
                _rigidbody.velocity = new Vector2(_rigidbody.velocity.x, Mathf.Clamp(_rigidbody.velocity.y, -1, 1));
                canWallJump = true;
            }
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        canWallJump = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.ToLower() == "traps")
        {
            Kill();
        }
    }
}
