﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class Timer : MonoBehaviour
{
    Text _textLabel;
    float _totalAmount = 0;
    // Start is called before the first frame update
    void Start()
    {
        _textLabel = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        _totalAmount += Time.deltaTime;
        _textLabel.text = Mathf.Floor(_totalAmount / 60) + ":" + (_totalAmount % 60).ToString("00.000");
    }
}
