﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowScript : MonoBehaviour
{
    [SerializeField]
    public GameObject _targetFollow;
    bool _ismovingStart = false;

    // Update is called once per frame
    void Update()
    {
        if (_targetFollow == null || _ismovingStart)
        {
            return;
        }
            transform.position = _targetFollow.transform.position + (Vector3.back * 10);
    }

    public void SetPlayer(Player player)
    {
        _ismovingStart = true;
        _targetFollow = player.gameObject;
        StartCoroutine(MoveToPlayer());
    }

    public IEnumerator MoveToPlayer()
    {
        Vector3 initialpos = transform.position;
        float timer = 0;

        while (timer < 1)
        {
            transform.position = Vector3.Lerp(initialpos, _targetFollow.transform.position, timer) + (Vector3.back * 10);
            timer += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        _ismovingStart = false;
    }
}
