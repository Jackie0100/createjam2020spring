﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class lifeorb : MonoBehaviour
{
    public Image orbimage;
    public float startingMana = 100;
    public float currentMana;
    

    // Start is called before the first frame update
    void Start()
    {
        currentMana = startingMana;
        InvokeRepeating("decreaseMana", 0f, 0.2f);
        
    }

    void decreaseMana()
    {
        currentMana -= 2;
        float calc_mana = currentMana / startingMana;
        SetMana(calc_mana);
    }

    void SetMana(float myMana)
    {
        orbimage.fillAmount = myMana;
    }

    // Update is called once per frame
    void Update()
    {
       
    }
}
