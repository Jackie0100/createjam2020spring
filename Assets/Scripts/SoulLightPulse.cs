﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class SoulLightPulse : MonoBehaviour
{
    float _startIntensity;
    [SerializeField]
    float _span = 0.2f;
    [SerializeField]
    float _speed = 1f;


    // Start is called before the first frame update
    void Start()
    {
        _startIntensity = this.GetComponent<Light2D>().intensity;
    }

    // Update is called once per frame
    void Update()
    {
        this.GetComponent<Light2D>().intensity = (_startIntensity - _span) + (_span * 2 * Mathf.PerlinNoise(Time.time * _speed, 0));
    }
}
