﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField]
    GameObject _player;

    [Button]
    public void Respawn()
    {
        Instantiate(_player, transform.position, Quaternion.identity);
    }

    [Button]
    public void DelayedRespawn(float time)
    {
        Invoke("Respawn", time);
    }
}
